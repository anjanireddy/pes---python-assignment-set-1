print("printing 1 to 100 using for loop")

for i in range(1,101):
    print(i)
    
print("printing 100 to 1 using for loop")
for i in range(100,0,-1):
    print(i)
    
print("printing 1 to 100 using while loop")

i = 1;
while i <= 100:
    print(i)
    i += 1
    
print("printing 100 to 1 using while loop")

i = 100;
while i >= 1:
    print(i)
    i -=1
    
print("Print each character of 'hello world' in separate lines")
mystring = "hello world"
for i in list(mystring):
    print(i)
    
