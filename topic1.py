def add(x, y):
	print("sum is {}".format(x+y))

def sub(x, y):
	if x>=y:
		print("difference is {}".format(x-y))
	else:
		print("difference is {}".format(y-x))

def mul(x, y):
	print("Product is {}".format(x*y))

def div(x, y):
	try:
		z = x/y
	except ZeroDivisionError:
		print("Can't divide by zero!")
	else:
		print("Quotient is {}".format(z))
try:
	num1 = int(input("Enter first number: "))
	num2 = int(input("Enter second number: "))
except ValueError:
	print("PLease make sure you are entering a number")
else:
	add(num1, num2)
	sub(num1, num2)
	mul(num1, num2)
	div(num1, num2)
